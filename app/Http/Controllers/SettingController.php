<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    //settings page
    function index()
    {
        $setting= Setting::first();
        return view('backend.setting.index',[
            'title'=>'Settings',
            'setting'=>$setting
        ]);
    }

    //handle form
    function save_settings(Request $request)
    {
        $countData= Setting::count();
        if ($countData==0)
        {
            $data=new Setting();
            $data->comment__auto=$request->comment_auto;
            $data->user_auto=$request->user_auto;
            $data->recent_limit=$request->recent_limit;
            $data->popular_limit=$request->popular_limit;
            $data->recent_comment_list=$request->recent_comment_list;

            $data->save();
        }
        else
        {
            $firstData= Setting::first();
            $data=Setting::find($firstData->id);
            $data->comment__auto=$request->comment_auto;
            $data->user_auto=$request->user_auto;
            $data->recent_limit=$request->recent_limit;
            $data->popular_limit=$request->popular_limit;
            $data->recent_comment_list=$request->recent_comment_list;

            $data->save();
        }

        return redirect('admin/setting')->with('success','Setting is successfull'); 
    }
}
