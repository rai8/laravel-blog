<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //render login page
    function login()
    {
        return view('backend.login');
    }

    //handle login
    function submit_login(Request $request)
    {
        $request->validate([
            'username'=>'required',
            'password'=>'required'
        ]);
        
        #check if user exists 
        $userCheck= Admin::where(['username'=>$request->username,'password'=>$request->password])->count();
        if($userCheck>0)
        {
            $adminData= Admin::where(['username'=>$request->username,'password'=>$request->password])->first();
            //setting session for admin-- session name is admin  data
            session(['adminData'=>$adminData]);

            return redirect('/admin/dashboard');
        }
        else
        {
            return redirect('/admin/login')->with('error','Invalid username/password !!!');
        }
    }

    //render admmin dashboard
    function dashboard()
    {
        return view('backend.dashboard');
    }

    //handle logout
    function logout()
    {
        session()->forget(['adminData']);
        return redirect('/admin/login');
    }
}
