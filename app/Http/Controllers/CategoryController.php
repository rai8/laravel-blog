<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all categories
        $data= Category::all();

        //show all categories page
        return view('backend.category.index', [
            'data'=>$data,
            'title'=>'All Categories',
            'meta_desc'=>'This is metadata for all categories'
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.category.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //post category to database
        $request->validate([
            'title'=>'required',
            'image'=>'mimes:jpeg, bmp,png,jpeg'
        ]);
        if($request->hasFile('cat_image'))
        {   
            $filenameWithExt = $request->file('cat_image')->getClientOriginalName ();
            $img_name = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $image= $request->file('cat_image');
            $fileName= $img_name.'_'.time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/imgs'), $fileName);



        }
        $category=new Category;
        $category->title=$request->title;
        $category->detail=$request->detail;
        $category->image=$fileName;
        $category->save();

        return redirect('admin/category/create')->with('success','Category added successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data= Category::find($id);
        return view('backend.category.update', [
            'data'=>$data
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'title'=>'required',
            'image'=>'mimes:jpeg, bmp,png,jpeg'
        ]);
        if($request->hasFile('cat_image'))
            {   
                $filenameWithExt = $request->file('cat_image')->getClientOriginalName ();
                $img_name = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $image= $request->file('cat_image');
                $fileName= $img_name.'_'.time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('/imgs'), $fileName);

            }
        else 
            {
                $fileName=$request->cat_image;
            }
        $category=Category::find($id);
        $category->title=$request->title;
        $category->detail=$request->detail;
        $category->image=$fileName;
        $category->save();

        return redirect('admin/category/'.$id.'/edit')->with('success','Category updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category= Category::where('id',$id)->delete();
        return redirect('admin/category');

    }
}
