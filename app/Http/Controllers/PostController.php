<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show all posts
        $data= Post::all();

        //show all categories page
        return view('backend.post.index', [
            'data'=>$data,
            'title'=>'All Posts',
            'meta_desc'=>'This is metadata for all posts'
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cats= Category::all();
        return view('backend.post.add',[
            'cats'=>$cats
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title'=>'required',
            'category'=>'required',
            'detail'=>'required',
            'thumb'=>'mimes:jpeg, bmp,png,jpeg',
            'full_img'=>'mimes:jpeg, bmp,png,jpeg',

        ]);

        //post for thumbnail
        if($request->hasFile('post_thumb'))
        {   
            $filenameWithExt = $request->file('post_thumb')->getClientOriginalName ();
            $img_name = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $image= $request->file('post_thumb');
            $thumbImage= $img_name.'_'.time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/imgs'), $thumbImage);

        }

        //post for full image
        if($request->hasFile('post_image'))
        {   
            $filenameWithExt = $request->file('post_image')->getClientOriginalName ();
            $img_name = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $image= $request->file('post_image');
            $fullImage= $img_name.'_'.time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/imgs'), $fullImage);

        }
        $post=new Post;
        $post->user_id=1;
        $post->cat_id=$request->category;
        $post->title=$request->title;
        $post->thumb=$thumbImage;
        $post->full_img=$fullImage;
        $post->detail=$request->detail;
        $post->tags=$request->tags;

        $post->save();

        return redirect('admin/post/create')->with('success','Post added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cats= Category::all();
        $data= Post::find($id);
        return view('backend.post.update', [
            'data'=>$data,
            'cats'=>$cats
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'title'=>'required',
            'category'=>'required',
            'detail'=>'required',
            'thumb'=>'mimes:jpeg, bmp,png,jpeg',
            'full_img'=>'mimes:jpeg, bmp,png,jpeg',

        ]);

        //post for thumbnail
        if($request->hasFile('post_thumb'))
            {   
                $filenameWithExt = $request->file('post_thumb')->getClientOriginalName ();
                $img_name = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $image= $request->file('post_thumb');
                $thumbImage= $img_name.'_'.time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('/imgs'), $thumbImage);

            }
        else
            {
                $thumbImage=$request->post_thumb;
            }

        //post for full image
        if($request->hasFile('post_image'))
            {   
                $filenameWithExt = $request->file('post_image')->getClientOriginalName ();
                $img_name = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $image= $request->file('post_image');
                $fullImage= $img_name.'_'.time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('/imgs'), $fullImage);

            }
            else
            {
                $fullImage=$request->post_image;
            }
        $post=Post::find($id);
        $post->user_id=1;
        $post->cat_id=$request->category;
        $post->title=$request->title;
        $post->thumb=$thumbImage;
        $post->full_img=$fullImage;
        $post->detail=$request->detail;
        $post->tags=$request->tags;

        $post->save();

        return redirect('admin/post/create')->with('success','Post updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post= Post::where('id', $id);
        $post->delete();
        return redirect('admin/post');

    }
}
