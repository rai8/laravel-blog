<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    function home()
    {
        // $posts= Post::orderBy('id', 'desc')->get(); without pagination
        $posts= Post::orderBy('id', 'desc')->paginate(3);
        return view('home',[
            'posts'=>$posts,
            'title'=>'RaiWriters'

        ]);
    }
}
