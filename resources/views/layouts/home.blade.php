
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard / Overview</li>
            </ol>
        
            @yield('content')
        </div>
    </main>
 @include('includes.footer')
</div>
</div>