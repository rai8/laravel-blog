<!DOCTYPE html>
<html lang="en">

    <head>
        @include('includes.head')
    </head>
    @if (!Session::has('adminData'))
    <script type="text/javascript">
        window.location.href="{{url('admin/login')}}";
    </script>     
    @endif
<body>
    {{-- sidebar component --}}
    @include('includes.header')

    {{-- sidebar component --}}
    @include('includes.sidebar')

    
    @extends('layouts.home')
    @section('content')
    
    <div class="card mb-3">
     <div class="card-header">
         <i class="fas fa-table me-1"></i>
         Update a Category
         <a href="{{url('admin/category')}}" class="float-right btn btn-sm btn-dark">All Data</a>
     </div>
     <div class="card-body">
        <div class="table-responsive">
            @if ($errors)
                @foreach ($errors->all() as $error)
                    <p class="text-danger">{{$error}}</p>
                @endforeach
            @endif
            @if (Session::has('success'))
                <p class="text-success">{{session('success')}}</p>
            @endif
        <form method="POST" action="{{url('admin/category/'.$data->id)}}" enctype="multipart/form-data">
            @csrf
            @method('put')
         <table class="table table-bordered">
            
                 <tr>
                     <th>Title</th>
                     <td><input type="text" value="{{$data->title}}" name="title" class="form-control" /></td>
                 </tr>
                 <tr>
                    <th>Detail</th>
                    <td><input type="text" value="{{$data->detail}}" name="detail" class="form-control" /></td>
                </tr>
                <tr>
                    <th>Image</th>
                    <td>
                    <p class="my-2">
                        <img src="{{ asset('imgs')}}/{{$data->image}}" width="50" height="50" />
                    </p>
                        <input type="file" name="cat_image" class="form-control" /></td>
                        <input type="hidden" value="{{$data->image}}" name="cat_image">
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" class="btn btn-primary" value="Update Category">
                    </td>
                </tr>
            
         </table>
        </form>
        </div>
     </div>
     <div class="card-footer small text-muted">Updated yesterday at 11:59pm</div>
    </div>
    @endsection

    
    
