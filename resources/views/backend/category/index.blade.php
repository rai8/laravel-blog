<!DOCTYPE html>
<html lang="en">

    <head>
        @include('includes.head')
    </head>
    @if (!Session::has('adminData'))
    <script type="text/javascript">
        window.location.href="{{url('admin/login')}}";
    </script>     
    @endif
<body>
    {{-- sidebar component --}}
    @include('includes.header')

    {{-- sidebar component --}}
    @include('includes.sidebar')

    
    @extends('layouts.home')
    @section('content')
    
    <div class="card mb-4">
     <div class="card-header">
         <i class="fas fa-table me-1"></i>
         Add Category
         <a href="{{url('admin/category/create')}}" class="float-right btn btn-sm btn-dark">Add Category</a>
     </div>
     <div class="card-body">
         <table class="table">
             <thead class="thead-dark">
                 <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Detail</th>
                    <th>Image</th>
                    <th>Action</th>
                     
                 </tr>
             </thead>
             <tfoot>
                 <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Detail</th>
                    <th>Image</th>
                    <th>Action</th>
                 </tr>
             </tfoot>
             <tbody>
                 @foreach ($data as $cat)
                 <tr>
                    <td>{{$cat->id}}</td>  
                    <td>{{$cat->title}}</td>  
                    <td>{{$cat->detail}}</td>  
                    <td><img src="{{ asset('imgs').'/'.$cat->image}}" width="50" height="50" /></td>  
                    <td>  
                        <a class="btn btn-info btn-sm" href="{{url('admin/category/'.$cat->id. '/edit')}}">Update</a>
                        <a onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger btn-sm" href="{{url('admin/category/'.$cat->id. '/delete')}}">Delete</a>
                    </td>  




                </tr> 
                 @endforeach          

             </tbody>
         </table>
     </div>
    </div>
    @endsection

    
    
