<!DOCTYPE html>
<html lang="en">

    <head>
        @include('includes.head')
    </head>
    @if (!Session::has('adminData'))
        <script type="text/javascript">
            window.location.href="{{url('admin/login')}}";
        </script>     
    @endif

<body>
    {{-- sidebar component --}}
    @include('includes.header')

    {{-- sidebar component --}}
    @include('includes.sidebar')

    
    @extends('layouts.home')
    @section('content')
    
    <div class="card mb-4">
     <div class="card-header">
         <i class="fas fa-table me-1"></i>
         Posts
         <a href="{{url('admin/post/create')}}" class="float-right btn btn-sm btn-dark">Add Post</a>
     </div>
     <div class="card-body">
         <table class="table">
             <thead class="thead-dark">
                 <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Detail</th>
                    <th>Thumbnail</th>
                    <th>Image</th>
                    <th>Action</th>
                     
                 </tr>
             </thead>
             <tfoot>
                 <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Detail</th>
                    <th>Thumbnail</th>
                    <th>Image</th>
                    <th>Action</th>
                 </tr>
             </tfoot>
             <tbody>
                 @if (count($data) > 0)
                 @foreach ($data as $post)
                <tr>
                    <td>{{$post->id}}</td>  
                    <td>{{$post->title}}</td>  
                    <td>{{$post->detail}}</td>  
                    <td><img src="{{ asset('imgs').'/'.$post->thumb}}" width="50" height="50" /></td> 
                    <td><img src="{{ asset('imgs').'/'.$post->full_img}}" width="50" height="50" /></td>  
                    <td>  
                        <a class="btn btn-info btn-sm" href="{{url('admin/post/'.$post->id. '/edit')}}">Update</a>
                        <a onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger btn-sm" href="{{url('admin/post/'.$post->id. '/delete')}}">Delete</a>
                    </td> 
                </tr> 
                @endforeach          
                 @else
                 <tr>
                    <td class="text-center">No data availble</td>  
                </tr>
                 @endif
                 

             </tbody>
         </table>
     </div>
    </div>
    @endsection

    
    
