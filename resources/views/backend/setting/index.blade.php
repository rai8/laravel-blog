<!DOCTYPE html>
<html lang="en">

    <head>
        @include('includes.head')
    </head>
    @if (!Session::has('adminData'))
    <script type="text/javascript">
        window.location.href="{{url('admin/login')}}";
    </script>     
    @endif
<body>
    {{-- sidebar component --}}
    @include('includes.header')

    {{-- sidebar component --}}
    @include('includes.sidebar')

    
    @extends('layouts.home')
    @section('content')

    <table class="table table-bordered">
        @if ($errors)
                @foreach ($errors->all() as $error)
                    <p class="text-danger">{{$error}}</p>
                @endforeach
            @endif
            @if (Session::has('success'))
                <p class="text-success">{{session('success')}}</p>
            @endif
        <form method="POST" action="{{url('admin/setting')}}" enctype="multipart/form-data">
            @csrf
        
             <tr>
                 <th>Comment Auto Approve</th>
                 <td><input type="text" @if($setting) value="{{$setting->comment__auto}}" @endif name="comment_auto" class="form-control" /></td>
             </tr>
             <tr>
                <th>User Auto Approve</th>
                <td><input type="text" @if($setting) value="{{$setting->user_auto}}" @endif name="user_auto" class="form-control" /></td>
            </tr>
            <tr>
                <th>Recent Post Limit</th>
                <td><input type="text" @if($setting) value="{{$setting->recent_limit}}" @endif name="recent_limit" class="form-control" /></td>
            </tr>
            <tr>
                <th>Popular Post Limit</th>
                <td><input type="text" @if($setting) value="{{$setting->popular_limit}}" @endif name="popular_limit" class="form-control" /></td>
            </tr>
            <tr>
                <th>Recent Comments Limit</th>
                <td><input type="text" @if($setting) value="{{$setting->recent_comment_list}}" @endif name="recent_comment_list" class="form-control" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="btn btn-primary">
                </td>
            </tr>
        
        </form>
    </table>
   
    @endsection

    
    
