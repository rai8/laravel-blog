<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')
    </head>
    @if (!Session::has('adminData'))
        <script type="text/javascript">
            window.location.href="{{url('admin/login')}}";
        </script>     
    @endif
    <body>

        @include('includes.header')
        {{-- sidebar component --}}
        @include('includes.sidebar')

        {{-- end of sidebar component --}}
    {{-- @if (Session::has('adminData'))
        <h1>Admin logged in</h1>
    @endif   --}}
    @extends('layouts.home')

    @section('content')
    @include('includes.menu')
    <div class="card mb-4">
     <div class="card-header">
         <i class="fas fa-table me-1"></i>
         Add Category
     </div>
     <div class="card-body">
         <table class="table">
             <thead class="thead-dark">
                 <tr>
                     <th>Name</th>
                     <th>Position</th>
                     <th>Office</th>
                     <th>Age</th>
                     <th>Start date</th>
                     <th>Salary</th>
                 </tr>
             </thead>
             <tfoot>
                 <tr>
                     <th>Name</th>
                     <th>Position</th>
                     <th>Office</th>
                     <th>Age</th>
                     <th>Start date</th>
                     <th>Salary</th>
                 </tr>
             </tfoot>
             <tbody>
                 <tr>
                     <td>Tiger Nixon</td>
                     <td>System Architect</td>
                     <td>Edinburgh</td>
                     <td>61</td>
                     <td>2011/04/25</td>
                     <td>$320,800</td>
                 </tr>
                 <tr>
                     <td>Garrett Winters</td>
                     <td>Accountant</td>
                     <td>Tokyo</td>
                     <td>63</td>
                     <td>2011/07/25</td>
                     <td>$170,750</td>
                 </tr>
                 <tr>
                     <td>Ashton Cox</td>
                     <td>Junior Technical Author</td>
                     <td>San Francisco</td>
                     <td>66</td>
                     <td>2009/01/12</td>
                     <td>$86,000</td>
                 </tr>
                 <tr>
                     <td>Cedric Kelly</td>
                     <td>Senior Javascript Developer</td>
                     <td>Edinburgh</td>
                     <td>22</td>
                     <td>2012/03/29</td>
                     <td>$433,060</td>
                 </tr>
           

             </tbody>
         </table>
     </div>
    </div>
    @endsection

