<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')
    </head>
  <!--Navbar Section  -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
        <div class="container">
          <a class="navbar-brand" href="{{url('/')}}">RaiWriters</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav ml-auto ">
              <a class="nav-link" href="#">Home</a>
              <a class="nav-link" href="#">Categories</a>
              <a class="nav-link" href="#">My Account</a>
      
            </div>
          </div>
        </div>
      </nav>
<!--Get latest posts  -->
<main class="container mt-4">
  <div class="row">
    <!--Left side posts section  -->
    <div class="col-md-8">
      <div class="row mb-5">
          @if(count($posts)>0)
          @foreach ($posts as $post)
            <div class="col-md-4">
            <div class="card">
              <a href="{{url('detail/'.$post->id)}}" >
                <img src="{{ asset('imgs')}}/{{$post->thumb}}" class="card-img-top" alt="Thumbnail">
              </a>
                <div class="card-body">
                  <h5 class="card-title">
                    <a class="text-decoration-none" href="{{url('detail/'.$post->id)}}">{{$post->title}}</a></h5>
              
                </div>
              </div>
            </div> 
          @endforeach
          @else
            <p class="alert alert-danger">No posts found
          @endif
      </div>
      <!--Add pagination  -->
      {{$posts->links()}}
   </div>
     <!--Right sidebar  -->
   <div class="col-md-4">
      <!--Search sidebar  -->
      <div class="card mb-3 shadow">
       <h5 class="card-header">Search</h1>
        <div class="card-body">
          <div class="input-group">
            <input type="text" class="form-control">
            <span class="btn btn-dark" id="basic-addon2">search</span>
          </div>
        </div>
      </div>

      <!--Recent posts  -->
      <div class="card mb-3 shadow">
        <h5 class="card-header">Recent posts</h1>
         <div class="list-group list-group-flush">
           @if ($recent_posts)
           @foreach ($recent_posts as $post)
            <a class="list-group-item text-decoration-none" href="#">{{$post->title}}</a>
            
           @endforeach
               
           @endif
           

         </div>
      </div>

      <!--Popular posts  -->
      <div class="card mb-3 shadow">
        <h5 class="card-header">Popular posts</h1>
         <div class="list-group list-group-flush">
           <a class="list-group-item" href="#">Post 1</a>
           <a class="list-group-item" href="#">Post 1</a>

         </div>
      </div>
   </div>
  </div>
</main>

</html>